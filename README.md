# text-tree

**Java library that draws trees in the console.**

Just simple ASCII:
```
build
+--- classes
+--- docs
|    `--- javadoc
+--- libs
+--- maven-repo-test
+--- reports
|    +--- checkstyle
|    +--- jacoco
|    |    `--- test
|    |         `--- html
|    `--- tests
|         `--- test
+--- resources
`--- tmp
```

Or Unicode:
```
build
├─── classes
├─── docs
│    ╰─── javadoc
├─── libs
├─── maven-repo-test
├─── reports
│    ├─── checkstyle
│    ├─── jacoco
│    │    ╰─── test
│    │         ╰─── html
│    ╰─── tests
│         ╰─── test
├─── resources
╰─── tmp
```

Even colored, and/or with annotations:

![Example Tree with Colors an Annotations](README-0.png)

... and all of it customizable, in a tiny library with very few dependencies.

---

**Contents**

[[_TOC_]]


## Get Started

*Text-tree* is published on Maven Central and jcenter, so you can just use it in your build:

Gradle:

```groovy
implementation 'org.barfuin.texttree:text-tree:0.9.4'
```

Maven:

```xml
<dependency>
  <groupId>org.barfuin.texttree</groupId>
  <artifactId>text-tree</artifactId>
  <version>0.9.4</version>
</dependency>
```


## Usage

Provided you already have a tree of objects to display, just add the
[Node](https://barfuin.gitlab.io/text-tree/latest/index.html?org/barfuin/texttree/api/Node.html) interface
to your tree objects, and then render the tree like so:

```java
Node tree = whereeverYourTreeComesFrom();
String rendered = TextTree.newInstance().render(tree);
System.out.println(rendered);    // or whatever else you want to do with the rendered tree
```

If you don't have a tree yet, you may use our
[DefaultNode](https://barfuin.gitlab.io/text-tree/latest/index.html?org/barfuin/texttree/api/DefaultNode.html) class
to construct one:

```java
DefaultNode tree = new DefaultNode("root");
tree.addChild(new DefaultNode("child1"));
tree.addChild(new DefaultNode("child2"));
```


## Details

The *text-tree* API is documented via [Javadoc](https://barfuin.gitlab.io/text-tree/latest/).

This is not a thread-safe API, so if your application has multiple threads creating trees, make sure each thread gets
its own instance of *text-tree*. If you are not sure what this means, you are probably not multi-threaded, and can
safely ignore this paragraph.


### The Elements of a Tree

A tree drawn by *text-tree* consists of these elements:

![The Elements of a Tree](README-2.png)

- *Structure* (yellow in the example above) — The tree structure, or just "the structure", are the lines which make the
  whole thing look like a tree.
- *Node text* (white in the example above) — The text shown for each node may also consist of more than one line. The
  structure will adjust.
- *Annotations* (gray in the example above) — A node may have an annotation, which can be rendered in a different color.
  The tree options determine where the annotation is printed. Annotations may also consist of more than one line. The
  structure will adjust.
- *Callouts* (red in the example above) — Callouts are messages added by *text-tree*. There are two kinds of callouts:
  errors and notes. *Errors* indicate things that shouldn't have happened. In the example above, an infinite loop was
  detected in the data. *Notes* indicate relevant information which is expected, for example when the maximum depth of
  a tree was exceeded, so some deeper nodes will be skipped.


### Tree Options

By passing
[TreeOptions](https://barfuin.gitlab.io/text-tree/latest/index.html?org/barfuin/texttree/api/TreeOptions.html)
to the factory method, you can control how the tree is rendered. For example, select a different tree style and limit
the tree's depth to 5 levels:

```java
TreeOptions options = new TreeOptions();
options.setStyle(TreeStyles.UNICODE_ROUNDED);
options.setMaxDepth(5);
String rendered = TextTree.newInstance(options).render(tree);
```

The rest of the tree options are explained in the Javadoc, and some of them below as we look at the different
features.


### Custom Tree Style

A few tree styles are already provided in the
[TreeStyles](https://barfuin.gitlab.io/text-tree/latest/index.html?org/barfuin/texttree/api/style/TreeStyles.html)
class. In addition to those, you can define your own custom tree style. In order to do that, you must provide the
following information (numbers explained below):

![Custom Tree Style Elements](README-1.png)

1. the text fragment used for continuing a higher level to the left of the current node
2. the text fragment for a junction in the tree (a child node)
3. the text fragment for a junction which is the last in its node (the last child of a node)
4. the text fragment printed in front of an annotation, normally an opening parenthesis
5. the text fragment printed after the end of an annotation, normally a closing parenthesis
6. the text fragment printed in front of a callout, normally an opening angle bracket
7. the text fragment printed after the end of a callout, normally a closing angle bracket

Items 1, 2, and 3 make up the heart of the tree style. These three elements must all have the same length in order to
make a proper tree.

The space to the right of the red boxes and before the node text is the *padding*. Its width is controlled by the
tree options as shown above.

The following code would create a tree style as shown above:

```java
TreeOptions options = new TreeOptions();
options.setStyle(new TreeStyle("│   ", "├───", "╰───", "(", ")", "<", ">"));
String rendered = TextTree.newInstance(options).render(tree);
```
In order to properly display a tree made of Unicode characters (as in this example), the terminal must support Unicode.


### Color Support

The trees drawn by this library can optionally be colored. Enable a simple coloring via `setEnableDefaultColoring()`:

```java
TreeOptions options = new TreeOptions();
options.setEnableDefaultColoring(true);
TextTree textTree = TextTree.newInstance(options);
```

This will activate the [default color
scheme](https://gitlab.com/barfuin/text-tree/-/blob/master/src/main/java/org/barfuin/texttree/api/color/DefaultColorScheme.java#L18).


#### A Note on the Use of Colors

Adding color to a Java console application is more difficult than it sounds, because of platform-specific differences,
and partially faulty color rendering by terminals. This is why you need to use a specialized library to emit the right
color codes.

Under the hood, we have [JAnsi](http://fusesource.github.io/jansi/) built in for that, and only little additional
abstraction as of now. So, it's probably saftest (although not required) if the rest of your application also used
JAnsi for coloring.


#### Custom Color Scheme

Custom color schemes may be defined by implementing the
[ColorScheme](https://barfuin.gitlab.io/text-tree/latest/index.html?org/barfuin/texttree/api/color/ColorScheme.html)
interface and activating it via the `setColorScheme()` tree option. If you are lazy, you can also just subclass the
default color scheme and change one or more of its colors. In the following example, we create a new color scheme which
is identical to the default color scheme, but features cyan colored annotations:

```java
public class MyOwnColorScheme extends DefaultColorScheme {
    @Override
    public NodeColor getAnnotationColor() {
        return NodeColor.LightCyan;
    }
}
```

### Cycle Protection

*text-tree* has built-in protection from cycles in your data which may otherwise lead to infinite loops. Consider,
for example, the case where a node appears as its own child node, as in `node.addChild(node);`. In such cases, we
will print a callout `<CYCLE>` and stop going down this branch.

Cycle protection "just works" by default, but there are two parameters that you can control via the tree options:
- the identity scheme
  ([IdentityScheme](https://barfuin.gitlab.io/text-tree/latest/index.html?org/barfuin/texttree/api/IdentityScheme.html) enum)
- the protection mode
  ([CycleProtection](https://barfuin.gitlab.io/text-tree/latest/index.html?org/barfuin/texttree/api/CycleProtection.html) enum)

The **identity scheme** specifies how *text-tree* determines the identity of a node. The default is to use [Java object
identity](https://stackoverflow.com/q/45453376/1005481) (`IdentityScheme.ByIdentity`), but you can also use the node
text (`IdentityScheme.ByText`), or a custom key that you define per node (`IdentityScheme.ByKey`). In the latter case,
make sure the `getKey()` methods of your nodes return meaningful keys.

The **protection mode** specifies when *text-tree* should stop printing nodes for a branch. The default is to avoid
infinite loops (`CycleProtection.On`), but you can also prune any repeating branches (`CycleProtection.PruneRepeating`),
or even turn cycle protection off completely (`CycleProtection.Off`).


### Translating Callouts

The text in callouts (`<CYCLE>`, `<shown before>`, or `<max. depth exceeded>`) adjusts to the locale of your JVM.
Currently, only a few languages are supported ([current
list](https://gitlab.com/barfuin/text-tree/-/tree/master/src/main/resources/org/barfuin/texttree/internal)). Please
submit a PR with a translation for your language, and I'll add it there! If you are new to GitLab, and "submitting a
PR" sounds too wild, just open an issue containing the new messages.properties in a localized version, and I'll add
it for you.

Please make sure to use the right ISO code in the file name, e.g. `messages_fr.properties` for the French version.
Inside the file, make sure the *keys* remain unmodified, and only the *values* of the entries are translated.
Thanks!


## Development

In order to develop text-tree and build it on your local machine, you need:

- Java 8 JDK ([download](https://adoptopenjdk.net/releases.html?variant=openjdk8))
- Gradle, but we use the Gradle Wrapper, so there is nothing to install for you
- Build with `./gradlew build`

The project is IDE agnostic. Just use your favorite IDE and make sure none of its control files get checked in.


## Status

text-tree is feature-complete and stable. It is ready for enterprise deployment.  
However, keep in mind that it's still a v0, some polishing is still going on, some of
which *may* lead to a breaking change in v1. No breaking changes are planned, though.


## License

text-tree is free software under the terms of the Apache License, Version 2.0.

Details in the [LICENSE.md](LICENSE.md) file.


## Contributing

Contributions of all kinds are very welcome! If you are going to spend a lot of time on your contribution, it may
make sense to raise an issue first and discuss your contribution. Thank you!
