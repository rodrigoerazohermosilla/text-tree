/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.barfuin.texttree.api.color.ColorScheme;
import org.barfuin.texttree.api.color.DefaultColorScheme;
import org.barfuin.texttree.api.color.NoColorScheme;
import org.barfuin.texttree.api.style.AnnotationPosition;
import org.barfuin.texttree.api.style.TreeStyle;
import org.barfuin.texttree.api.style.TreeStyles;


/**
 * Configuration options that apply to one complete rendered tree.
 */
public final class TreeOptions
{
    private static final IdentityScheme DEFAULT_IDENTITY_SCHEME = IdentityScheme.ByIdentity;

    private static final TreeStyle DEFAULT_TREE_STYLE = TreeStyles.ASCII_ROUNDED;

    private static final CycleProtection DEFAULT_CYCLE_PROTECTION = CycleProtection.On;

    private static final ColorScheme NO_COLOR_SCHEME = new NoColorScheme();

    private static final AnnotationPosition DEFAULT_ANNOT_POS = AnnotationPosition.NextLine;

    private static final int DEFAULT_MAX_DEPTH = 100;

    private IdentityScheme identityScheme = DEFAULT_IDENTITY_SCHEME;

    private String padding = " ";

    private TreeStyle style = DEFAULT_TREE_STYLE;

    private CycleProtection cycleProtection = DEFAULT_CYCLE_PROTECTION;

    private ColorScheme colorScheme = NO_COLOR_SCHEME;

    private AnnotationPosition annotationPosition = DEFAULT_ANNOT_POS;

    private int maxDepth = DEFAULT_MAX_DEPTH;



    @Nonnull
    public IdentityScheme getIdentityScheme()
    {
        return identityScheme;
    }



    public void setIdentityScheme(@Nullable final IdentityScheme pIdentityScheme)
    {
        identityScheme = pIdentityScheme != null ? pIdentityScheme : DEFAULT_IDENTITY_SCHEME;
    }



    public int getPadding()
    {
        return padding.length();
    }



    @Nonnull
    public String getPaddingStr()
    {
        return padding;
    }



    public void setPadding(final int pLenPadding)
    {
        if (pLenPadding < 0) {
            throw new IllegalArgumentException("padding must be 0 or positive, but was: " + pLenPadding);
        }
        padding = Stream.generate(() -> " ").limit(pLenPadding).collect(Collectors.joining());
    }



    @Nonnull
    public TreeStyle getStyle()
    {
        return style;
    }



    public void setStyle(@Nullable final TreeStyle pStyle)
    {
        style = pStyle != null ? pStyle : DEFAULT_TREE_STYLE;
    }



    @Nonnull
    public CycleProtection getCycleProtection()
    {
        return cycleProtection;
    }



    public void setCycleProtection(@Nullable final CycleProtection pCycleProtection)
    {
        cycleProtection = pCycleProtection != null ? pCycleProtection : DEFAULT_CYCLE_PROTECTION;
    }



    /**
     * Setter.
     *
     * @param pEnableDefaultColoring flag indicating whether the resulting tree should have default colors
     * (<code>true</code>) or not be colored at all (<code>false</code>). The default is <code>false</code>.
     * Individual node colors will <em>always</em> be displayed if set.
     * @see #setColorScheme
     */
    public void setEnableDefaultColoring(final boolean pEnableDefaultColoring)
    {
        colorScheme = pEnableDefaultColoring ? new DefaultColorScheme() : NO_COLOR_SCHEME;
    }



    @Nonnull
    public ColorScheme getColorScheme()
    {
        return colorScheme;
    }



    public void setColorScheme(@Nullable final ColorScheme pColorScheme)
    {
        colorScheme = pColorScheme != null ? pColorScheme : NO_COLOR_SCHEME;
    }



    @Nonnull
    public AnnotationPosition getAnnotationPosition()
    {
        return annotationPosition;
    }



    public void setAnnotationPosition(@Nullable final AnnotationPosition pAnnotationPosition)
    {
        annotationPosition = pAnnotationPosition != null ? pAnnotationPosition : DEFAULT_ANNOT_POS;
    }



    public int getMaxDepth()
    {
        return maxDepth;
    }



    /**
     * Setter.
     *
     * @param pMaxDepth the maximum depth of the resulting tree. Nodes which are lower down will be clipped. A value of
     * zero indicates unlimited depth.
     */
    public void setMaxDepth(final int pMaxDepth)
    {
        if (pMaxDepth < 0) {
            throw new IllegalArgumentException("max depth cannot be negative");
        }
        maxDepth = pMaxDepth;
    }
}
