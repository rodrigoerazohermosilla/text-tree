/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.barfuin.texttree.internal.TextTreeImpl;


/**
 * Use this type to render trees. 🌳🌲🌳
 */
public interface TextTree
{
    /**
     * Render the given tree into a String representation (synchronously).
     *
     * @param pNode a tree
     * @return the entire tree in a String representation, optionally colorized
     */
    @Nonnull
    String render(@Nullable final Node pNode);


    /**
     * Factory method.
     *
     * @param pOptions tree options
     * @return a TextTree instance that is ready to go
     */
    @Nonnull
    static TextTree newInstance(@Nullable final TreeOptions pOptions)
    {
        return new TextTreeImpl(pOptions);
    }


    /**
     * Factory method.
     *
     * @return a TextTree instance that is ready to go
     */
    @Nonnull
    static TextTree newInstance()
    {
        return new TextTreeImpl(null);
    }
}
