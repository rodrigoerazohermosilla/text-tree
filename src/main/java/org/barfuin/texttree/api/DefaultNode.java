/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.barfuin.texttree.api.color.NodeColor;


/**
 * A default implementation of the {@link Node} interface.
 */
public class DefaultNode
    implements Node
{
    private String text;

    private String key;

    private NodeColor colorOverride;

    private String annotation;

    private List<Node> children;



    /**
     * Constructor that initializes a node which contains the empty string as its node text.
     */
    public DefaultNode()
    {
        this("");
    }



    /**
     * Constructor.
     *
     * @param pText the node's text
     */
    public DefaultNode(final String pText)
    {
        this(pText, pText, null, null, null);
    }



    public DefaultNode(@Nullable final String pText, @Nullable final String pKey,
        @Nullable final NodeColor pColorOverride, @Nullable final String pAnnotation,
        @Nullable final List<Node> pChildren)
    {
        text = pText;
        key = pKey;
        colorOverride = pColorOverride;
        annotation = pAnnotation;
        children = pChildren != null ? pChildren : new ArrayList<>();
    }



    @Override
    @CheckForNull
    public String getKey()
    {
        return key;
    }



    public void setKey(@Nullable final String pKey)
    {
        key = pKey;
    }



    @Override
    @CheckForNull
    public String getText()
    {
        return text;
    }



    public void setText(@Nullable final String pText)
    {
        text = pText;
    }



    @Override
    @CheckForNull
    public NodeColor getColor()
    {
        return colorOverride;
    }



    public void setColor(@Nullable final NodeColor pColorOverride)
    {
        colorOverride = pColorOverride;
    }



    @Override
    @CheckForNull
    public String getAnnotation()
    {
        return annotation;
    }



    public void setAnnotation(@Nullable final String pAnnotation)
    {
        annotation = pAnnotation;
    }



    @Nonnull
    @Override
    public Iterable<Node> getChildren()
    {
        return children;
    }



    public void setChildren(@Nullable final List<Node> pChildren)
    {
        children = pChildren != null ? pChildren : new ArrayList<>();
    }



    public void addChild(@Nullable final Node pChild)
    {
        children.add(pChild);
    }
}
