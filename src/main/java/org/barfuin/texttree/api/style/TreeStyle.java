/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api.style;

import java.util.Objects;
import javax.annotation.Nonnull;


/**
 * Describes a tree style, giving all the text elements needed for rendering. This class is useful for implementing
 * custom tree styles. Pre-defined tree styles are available in the {@link TreeStyles} class.
 * <p>The README on the <a href="https://gitlab.com/barfuin/text-tree#custom-tree-style">project's website</a> has
 * detailed information about how to define custom tree styles.</p>
 *
 * @see TreeStyles
 */
public class TreeStyle
{
    private final String indent;

    private final String blankIndent;

    private final String junction;

    private final String lastJunction;

    private final String annotationStart;

    private final String annotationEnd;

    private final String calloutStart;

    private final String calloutEnd;



    /**
     * Constructor. All arguments are required and must be of equal length.
     *
     * @param pIndent the text fragment used for continuing a higher level to the left of the current node
     * @param pJunction the text fragment for a junction in the tree (a child node)
     * @param pLastJunction the text fragment for a junction which is the last in its node (the last child of a node)
     * @param pAnnotationStart text fragment printed in front of an annotation, normally an opening parenthesis
     * @param pAnnotationEnd text fragment printed after the end of an annotation, normally a closing parenthesis
     * @param pCalloutStart text fragment printed in front of a callout, normally an opening angle bracket
     * @param pCalloutEnd text fragment printed after the end of a callout, normally a closing angle bracket
     * @see TreeStyles
     */
    public TreeStyle(final String pIndent, final String pJunction, final String pLastJunction,
        final String pAnnotationStart, final String pAnnotationEnd, final String pCalloutStart,
        final String pCalloutEnd)
    {
        indent = Objects.requireNonNull(pIndent, "Argument pIndent must not be null");
        blankIndent = generateBlankIndent(pIndent);
        junction = Objects.requireNonNull(pJunction, "Argument pJunction must not be null");
        lastJunction = Objects.requireNonNull(pLastJunction, "Argument pLastJunction must not be null");
        annotationStart = Objects.requireNonNull(pAnnotationStart, "Argument pAnnotationStart must not be null");
        annotationEnd = Objects.requireNonNull(pAnnotationEnd, "Argument pAnnotation must not be null");
        calloutStart = Objects.requireNonNull(pCalloutStart, "Argument pCallout must not be null");
        calloutEnd = Objects.requireNonNull(pCalloutEnd, "Argument pCalloutEnd must not be null");

        if (pIndent.length() != pJunction.length() || pIndent.length() != pLastJunction.length()) {
            throw new IllegalArgumentException("element size mismatch");
        }
    }



    @Nonnull
    @SuppressWarnings("ReplaceAllDot")
    private String generateBlankIndent(@Nonnull final String pIndent)
    {
        return pIndent.replaceAll(".", " ");
    }



    @Nonnull
    public String getIndent()
    {
        return indent;
    }



    @Nonnull
    public String getBlankIndent()
    {
        return blankIndent;
    }



    @Nonnull
    public String getJunction()
    {
        return junction;
    }



    @Nonnull
    public String getLastJunction()
    {
        return lastJunction;
    }



    @Nonnull
    public String getAnnotationStart()
    {
        return annotationStart;
    }



    @Nonnull
    public String getAnnotationEnd()
    {
        return annotationEnd;
    }



    @Nonnull
    public String getCalloutStart()
    {
        return calloutStart;
    }



    @Nonnull
    public String getCalloutEnd()
    {
        return calloutEnd;
    }
}
