/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api.style;

import org.barfuin.texttree.api.TreeOptions;


/**
 * The pre-defined tree styles. Define custom styles by simply creating a {@link TreeStyle} instance yourself.
 */
public final class TreeStyles
{
    /**
     * A tree style made purely of ASCII characters, for maximum portability. The output looks just like the trees
     * drawn by the Gradle build tool when it displays dependency trees.
     * <pre> sample tree
     * +--- node (annotation)
     * |    +--- another node
     * |    \--- node &lt;CYCLE&gt;
     * \--- last node</pre>
     */
    public static final TreeStyle ASCII = new TreeStyle("|   ", "+---", "\\---", "(", ")", "<", ">");

    /**
     * Same as the <code>ASCII</code> tree style, but with nice rounded-off corners.
     * <pre> sample tree
     * +--- node (annotation)
     * |    +--- another node
     * |    `--- node &lt;CYCLE&gt;
     * `--- last node</pre>
     */
    public static final TreeStyle ASCII_ROUNDED = new TreeStyle("|   ", "+---", "`---", "(", ")", "<", ">");

    /**
     * A tree style made of Unicode line drawing characters, just like what the Windows <code>tree</code> command
     * produces. To have an identical output to the Windows <code>tree</code> command, set the
     * {@link TreeOptions#setPadding(int) padding} to zero in the tree options.
     * <pre> sample tree
     * ├─── node (annotation)
     * │    ├─── another node
     * │    └─── node ‹CYCLE›
     * └─── last node</pre>
     */
    public static final TreeStyle UNICODE = new TreeStyle("│   ", "├───", "└───", "(", ")", "‹", "›");

    /**
     * Same as the <code>UNICODE</code> tree style, but with nice rounded-off corners.
     * <pre> sample tree
     * ├─── node (annotation)
     * │    ├─── another node
     * │    ╰─── node ‹CYCLE›
     * ╰─── last node</pre>
     */
    public static final TreeStyle UNICODE_ROUNDED = new TreeStyle("│   ", "├───", "╰───", "(", ")", "‹", "›");



    private TreeStyles()
    {
        // prevent instantiation
    }
}
