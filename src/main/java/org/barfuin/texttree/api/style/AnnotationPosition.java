/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api.style;

/**
 * Where to place node annotations.
 */
public enum AnnotationPosition
{
    /**
     * The node annotation is displayed on the same line as the node text, e.g. <code>node text (annotation)</code>.
     */
    Inline,

    /** The node annotation is positioned in a new line following the node text. */
    NextLine;
}
