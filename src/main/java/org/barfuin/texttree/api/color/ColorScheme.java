/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api.color;

/**
 * Describes a color scheme for the entire tree. In addition to that, nodes can have individual colors, which would
 * take precedence if present.
 */
public interface ColorScheme
{
    /**
     * Getter.
     *
     * @return the color of edges in the tree
     */
    NodeColor getEdgeColor();


    /**
     * Getter.
     *
     * @return the color of the node text in the tree
     */
    NodeColor getTextColor();


    /**
     * Getter.
     *
     * @return the color of node annotations in the tree
     */
    NodeColor getAnnotationColor();


    /**
     * Getter.
     *
     * @return the color of callouts that signify <em>exceptional</em> conditions, such as a cycle in the tree
     */
    NodeColor getCalloutErrorColor();


    /**
     * Getter.
     *
     * @return the color of callouts which are just notes or hints, for example a repeating subtree was clipped
     */
    NodeColor getCalloutNoteColor();
}
