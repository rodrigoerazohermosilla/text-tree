/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api.color;

/**
 * The default color scheme for colored trees.
 */
public class DefaultColorScheme
    implements ColorScheme
{
    @Override
    public NodeColor getEdgeColor()
    {
        return NodeColor.DarkYellow;
    }



    @Override
    public NodeColor getTextColor()
    {
        return NodeColor.None;
    }



    @Override
    public NodeColor getAnnotationColor()
    {
        return NodeColor.DarkGray;
    }



    @Override
    public NodeColor getCalloutErrorColor()
    {
        return NodeColor.LightRed;
    }



    @Override
    public NodeColor getCalloutNoteColor()
    {
        return NodeColor.DarkGray;
    }
}
