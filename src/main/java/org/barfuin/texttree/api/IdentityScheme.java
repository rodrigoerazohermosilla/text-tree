/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api;

/**
 * How the internal cycle detector should identify objects.
 */
public enum IdentityScheme
{
    /**
     * A node is identified by the text that is being displayed for it ({@link Node#getText() getText()}),
     * so two nodes are considered to be the same node if they have the same text.
     */
    ByText,

    /**
     * A node is identified by its key ({@link Node#getKey() getKey()}),
     * so two nodes are considered to be the same node if they have the key.
     */
    ByKey,

    /**
     * A node is identified by its Java object identity,
     * so two nodes are considered to be the same node if they are actually the same Java object.
     */
    ByIdentity;
}
