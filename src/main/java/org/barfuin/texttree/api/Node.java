/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api;

import javax.annotation.CheckForNull;

import org.barfuin.texttree.api.color.NodeColor;


/**
 * A data node in the tree to be rendered. Implement this interface in the elements of your tree.
 *
 * @see DefaultNode
 */
public interface Node
{
    /**
     * The node's text. May contain line breaks.
     *
     * @return the node's text
     */
    @CheckForNull
    String getText();


    /**
     * An optional primary key for the node. This can be used to identify nodes for cycle detection. It defaults to
     * the node text if not overridden. The value returned from this method is used only if the
     * {@link IdentityScheme#ByKey} is explicitly selected. Otherwise, this method should be left to its default
     * implementation.
     * <p>If you actually use this value (because you've chosen {@link IdentityScheme#ByKey}), then you should make
     * sure that it not return <code>null</code>. All nodes which do that will be considered the same node then
     * (which may be what you want. I'm just saying).</p>
     *
     * @return an identifying String
     */
    @CheckForNull
    default String getKey()
    {
        return getText();
    }


    /**
     * Return a color here for colored nodes. The color set here takes precedence over the tree's color scheme.
     *
     * @return the node color, or <code>null</code> to use the color from the tree's color scheme. A value of
     * {@link NodeColor#None} indicates that the node <em>must not</em> be colored.
     */
    @CheckForNull
    default NodeColor getColor()
    {
        return null;
    }


    /**
     * Return a String here to display as a node annotation. May contain line breaks.
     *
     * @return the annotation, or <code>null</code> for no annotation
     */
    @CheckForNull
    default String getAnnotation()
    {
        return null;
    }


    /**
     * The node's child nodes. This method may be called multiple times, and it is expected to return the same
     * elements on each invocation. Nodes returned by this iterator may also be <code>null</code> (shouldn't, but we
     * can handle it).
     *
     * @return the list of child nodes, or <code>null</code> / empty if this is a leaf node
     */
    @CheckForNull
    Iterable<Node> getChildren();
}
