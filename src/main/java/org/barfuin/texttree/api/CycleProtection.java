/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api;

/**
 * How cycle protection is handled. This controls what happens when a node appears as its own child node, or when a
 * node appears in multiple place around the tree.
 */
public enum CycleProtection
{
    /**
     * We do nothing about cycles, every node is printed as-is. Use this only when you are very sure about your input
     * data not containing cycles. Be careful, stack overflows lurk here.
     */
    Off,

    /**
     * Cycles are detected according to the {@link IdentityScheme} and the tree is clipped when a cycle occurs on the
     * current path in the tree. In other words, when a node is its own child node at some point.
     * <p>When a reoccurring node is clipped in this mode, the node text is still printed, but not its annotation
     * or child nodes, and a callout <code>&lt;CYCLE&gt;</code> is appended to the node text.</p>
     */
    On,

    /**
     * In this mode, <em>any</em> repeating node will be clipped, even if the repeat occurrence is in a parallel
     * subtree. Repeating nodes are detected according to the {@link IdentityScheme}. This is useful if it should be
     * avoided to show repeating information.
     * <p>When a reoccurring node is clipped in this mode, the node text is still printed, but not its annotation
     * or child nodes, and a callout <code>&lt;shown before&gt;</code> is appended to the node text.</p>
     * <p>Cycles of the form described for {@link #On} can still occur in this mode, and will be flagged
     * accordingly.</p>
     */
    PruneRepeating;
}
