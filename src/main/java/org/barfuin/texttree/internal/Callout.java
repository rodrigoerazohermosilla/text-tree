/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.util.Locale;
import java.util.ResourceBundle;
import javax.annotation.Nonnull;


/**
 * The possible callouts and their metadata. Callouts are not annotations, but extra information added by the tree
 * rendering algorithm.
 */
public enum Callout
{
    /** no callout is displayed - the metadata are dummy values */
    None(TreeElementType.CalloutError, false),

    /** a subtree that was already printed before was pruned this time */
    RepeatingNode(TreeElementType.CalloutNote, true),

    /** a cycle was detected, i.e. a node was encountered who is a parent of itself */
    Cycle(TreeElementType.CalloutError, true),

    /** the maximum configured tree depth was exceeded */
    MaxDepth(TreeElementType.CalloutNote, false);

    //

    private final TreeElementType elementType;

    private final boolean printOnSameLine;



    private Callout(@Nonnull final TreeElementType pElementType, final boolean pPrintOnSameLine)
    {
        elementType = pElementType;
        printOnSameLine = pPrintOnSameLine;
    }



    @Nonnull
    public TreeElementType getElementType()
    {
        return elementType;
    }



    @Nonnull
    public String getText()
    {
        if (this == None) {
            return "";
        }
        return ResourceBundle.getBundle("org.barfuin.texttree.internal.messages")
            .getString("callout." + name().toLowerCase(Locale.ENGLISH));
    }



    public boolean isPrintOnSameLine()
    {
        return printOnSameLine;
    }
}
