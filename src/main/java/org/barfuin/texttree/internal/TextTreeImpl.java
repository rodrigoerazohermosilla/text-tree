/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.util.Iterator;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;

import org.barfuin.texttree.api.Node;
import org.barfuin.texttree.api.TextTree;
import org.barfuin.texttree.api.TreeOptions;
import org.barfuin.texttree.api.style.AnnotationPosition;
import org.barfuin.texttree.api.style.TreeStyle;


/**
 * Renders a text tree.
 *
 * <p>This class is not thread-safe. Use a distinct instance in each thread, if you do multi-threaded tree generation
 * at all 😉.</p>
 */
@NotThreadSafe
public class TextTreeImpl
    implements TextTree
{
    private final TreeOptions options;

    private ColorizableAppender appender = null;

    private CycleDetector cycleDetector = null;



    public TextTreeImpl()
    {
        this(null);
    }



    public TextTreeImpl(@Nullable final TreeOptions pOptions)
    {
        options = pOptions != null ? pOptions : new TreeOptions();
    }



    @Nonnull
    @Override
    public String render(@Nullable final Node pNode)
    {
        init();

        if (pNode != null) {
            render("", 0, pNode);
        }
        else {
            appender.appendText(null, null);
        }
        return appender.finishUp();
    }



    private void init()
    {
        appender = new ColorizableAppender(options);
        cycleDetector = new CycleDetector(options.getCycleProtection(), options.getIdentityScheme());
    }



    @SuppressWarnings("ConstantConditions")
    private void render(@Nonnull final String pPrefix, final int pLevel, @Nullable final Node pNode)
    {
        final TreeStyle style = options.getStyle();
        printNodeText(pPrefix, pNode);

        final Callout callout = cycleDetector.visit(pNode);
        if (callout != Callout.None) {
            printCallout(callout);
            return;
        }

        if (options.getAnnotationPosition() == AnnotationPosition.NextLine
            || pNode == null || pNode.getAnnotation() == null)
        {
            appender.newLine();
        }
        if (pNode != null) {
            printAnnotation(pPrefix, pNode.getAnnotation());

            if (hasChildren(pNode)) {
                for (Iterator<Node> iter = pNode.getChildren().iterator(); iter.hasNext(); ) {
                    final boolean maxDepthExceeded = options.getMaxDepth() > 0 && pLevel >= options.getMaxDepth();
                    final Node child = iter.next();

                    appender.append(TreeElementType.Edge, pPrefix);
                    appender.append(TreeElementType.Edge,
                        iter.hasNext() && !maxDepthExceeded ? style.getJunction() : style.getLastJunction());
                    appender.append(TreeElementType.Edge, options.getPaddingStr());

                    if (maxDepthExceeded) {
                        printCallout(Callout.MaxDepth);
                        break;
                    }
                    else {
                        render(pPrefix
                                + (iter.hasNext() ? style.getIndent() : style.getBlankIndent())
                                + options.getPaddingStr(),
                            pLevel + 1, child);
                    }
                }
            }
        }
        cycleDetector.pop();
    }



    private void printNodeText(@Nonnull final String pPrefix, @Nullable final Node pNode)
    {
        final String text = pNode != null ? pNode.getText() : null;
        if (text != null) {
            final String[] lines = text.split(System.lineSeparator());
            for (int i = 0; i < lines.length; i++) {
                final String line = lines[i];
                if (i > 0) {
                    appender.append(TreeElementType.Edge, pPrefix);
                }
                appender.appendText(line, pNode.getColor());
                if (i < lines.length - 1) {
                    appender.newLine();
                }
            }
        }
        else {
            appender.appendText(null, pNode != null ? pNode.getColor() : null);
        }
    }



    private void printCallout(@Nonnull final Callout pCallout)
    {
        final TreeStyle style = options.getStyle();
        if (pCallout.isPrintOnSameLine()) {
            appender.appendText(" ", null);
        }
        appender.append(pCallout.getElementType(), style.getCalloutStart());
        appender.append(pCallout.getElementType(), pCallout.getText());
        appender.append(pCallout.getElementType(), style.getCalloutEnd());
        appender.newLine();
    }



    private void printAnnotation(@Nonnull final String pPrefix, @Nullable final String pAnnotation)
    {
        if (pAnnotation != null) {
            final TreeStyle style = options.getStyle();
            final String[] lines = pAnnotation.trim().split(System.lineSeparator());
            for (int i = 0; i < lines.length; i++) {
                final String line = lines[i];
                if (options.getAnnotationPosition() == AnnotationPosition.NextLine || i > 0) {
                    appender.append(TreeElementType.Edge, pPrefix);
                }
                if (options.getAnnotationPosition() == AnnotationPosition.Inline) {
                    if (i > 0) {
                        appender.append(TreeElementType.Annotation, "  ");  // continuation indent
                    }
                    else {
                        appender.append(TreeElementType.Text, " ");  // separate from node text on same line
                    }
                }

                if (i == 0) {
                    appender.append(TreeElementType.Annotation, style.getAnnotationStart());
                }
                appender.append(TreeElementType.Annotation, line);
                if (i == lines.length - 1) {
                    appender.append(TreeElementType.Annotation, style.getAnnotationEnd());
                }

                appender.newLine();
            }
        }
    }



    @SuppressWarnings("ConstantConditions")
    static boolean hasChildren(@Nullable final Node pNode)
    {
        boolean result = false;
        if (pNode != null && pNode.getChildren() != null) {
            Iterator<Node> iter = pNode.getChildren().iterator();
            return iter != null && iter.hasNext();
        }
        return result;
    }
}
