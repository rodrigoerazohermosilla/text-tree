/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.barfuin.texttree.api.CycleProtection;
import org.barfuin.texttree.api.IdentityScheme;
import org.barfuin.texttree.api.Node;


/**
 * Detects cycles in the tree. A cycle is detected when a node appears twice during depth-first traversal.
 * The configured {@link IdentityScheme} determines the strategy by which node identity is asserted.
 */
public class CycleDetector
{
    private final CycleProtection protectionMode;

    private final IdentityScheme identityScheme;

    /** set of all previously visited nodes by identifying information, for 'PruneRepeating' mode */
    private final Set<String> previousNodes = new HashSet<>();

    /** stack of nodes on the current path, by identifying information, for {@link CycleProtection#On On} mode */
    private final Deque<String> stack = new ArrayDeque<>();



    public CycleDetector(@Nonnull final CycleProtection pProtectionMode, @Nonnull final IdentityScheme pIdentityScheme)
    {
        protectionMode = Objects.requireNonNull(pProtectionMode, "Argument pProtectionMode must not be null");
        identityScheme = Objects.requireNonNull(pIdentityScheme, "Argument pIdentityScheme must not be null");
    }



    @Nonnull
    public Callout visit(@Nullable final Node pNode)
    {
        if (protectionMode == CycleProtection.Off) {
            return Callout.None;
        }

        Callout result = Callout.None;
        final String nodeId = getIdForNode(pNode);

        if (protectionMode == CycleProtection.PruneRepeating) {
            if (!isNullNode(pNode) && previousNodes.contains(nodeId)) {
                result = Callout.RepeatingNode;
            }
            else {
                previousNodes.add(nodeId);
            }
        }

        if (stack.contains(nodeId)) {
            result = Callout.Cycle;
        }
        else {
            stack.push(nodeId);
        }

        return result;
    }



    @Nonnull
    @SuppressWarnings("ConstantConditions")
    private String getIdForNode(@Nullable final Node pNode)
    {
        switch (identityScheme) {
            case ByKey:
                return isNullNode(pNode) ? "null" : pNode.getKey();
            case ByText:
                return isNullNode(pNode) ? "null" : pNode.getText();
            case ByIdentity:
                return (isNullNode(pNode) ? "null" : pNode.getClass().getName())
                    + "@" + Integer.toHexString(System.identityHashCode(pNode));
        }
        throw new IllegalStateException("Bug: Unhandled node identity scheme - " + identityScheme);
    }



    private boolean isNullNode(@Nullable final Node pNode)
    {
        boolean result = true;
        if (pNode != null) {
            if (identityScheme == IdentityScheme.ByKey) {
                result = pNode.getKey() == null;
            }
            else if (identityScheme == IdentityScheme.ByText) {
                result = pNode.getText() == null;
            }
            else {
                result = false;
            }
        }
        return result;
    }



    public void pop()
    {
        if (protectionMode != CycleProtection.Off) {
            stack.pop();
        }
    }
}
