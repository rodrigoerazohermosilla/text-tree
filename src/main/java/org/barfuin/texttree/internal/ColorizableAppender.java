/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.barfuin.texttree.api.TreeOptions;
import org.barfuin.texttree.api.color.NodeColor;
import org.fusesource.jansi.Ansi;


/**
 * Adds ANSI escape sequences to text for colorization.
 */
public class ColorizableAppender
{
    private final TreeOptions options;

    private final StringBuilder sb = new StringBuilder();

    private final Ansi ansi = Ansi.ansi(sb);

    private boolean colorWasUsed = false;

    private NodeColor latestColorUsed = NodeColor.None;



    public ColorizableAppender(final TreeOptions pTreeOptions)
    {
        options = Objects.requireNonNull(pTreeOptions, "tree options must not be null");
    }



    public void appendText(@Nullable final String pNodeText, @Nullable final NodeColor pColorOverride)
    {
        append(TreeElementType.Text, pNodeText, pColorOverride);
    }



    public void append(@Nonnull final TreeElementType pElementType, @Nonnull final String pString)
    {
        append(pElementType, pString, null);
    }



    public void append(@Nonnull final TreeElementType pElementType, @Nullable final String pString,
        @Nullable final NodeColor pColorOverride)
    {
        if (!"".equals(pString)) {
            final NodeColor effectiveColor = pColorOverride != null ? pColorOverride : getColorFromScheme(pElementType);
            if (!colorWasUsed && effectiveColor == NodeColor.None) {
                sb.append(pString);
            }
            else {
                emitColor(effectiveColor).a(pString);
                colorWasUsed = true;
            }
        }
    }



    @Nonnull
    private NodeColor getColorFromScheme(@Nonnull final TreeElementType pElementType)
    {
        NodeColor result;
        switch (pElementType) {
            case Edge:
                result = options.getColorScheme().getEdgeColor();
                break;
            case Text:
                result = options.getColorScheme().getTextColor();
                break;
            case CalloutError:
                result = options.getColorScheme().getCalloutErrorColor();
                break;
            case CalloutNote:
                result = options.getColorScheme().getCalloutNoteColor();
                break;
            case Annotation:
                result = options.getColorScheme().getAnnotationColor();
                break;
            default:
                throw new IllegalStateException("bug: unhandled element in color scheme - " + pElementType);
        }
        return result != null ? result : NodeColor.None;
    }



    @Nonnull
    private Ansi emitColor(@Nonnull final NodeColor pNodeColor)
    {
        if (pNodeColor != latestColorUsed) {
            switch (pNodeColor) {
                case None:
                    ansi.reset();
                    break;
                case Black:
                    ansi.fgBlack();
                    break;
                case White:
                    ansi.fgBright(Ansi.Color.WHITE);
                    break;
                case DarkRed:
                    ansi.fgRed();
                    break;
                case LightRed:
                    ansi.fgBrightRed();
                    break;
                case DarkGreen:
                    ansi.fgGreen();
                    break;
                case LightGreen:
                    ansi.fgBrightGreen();
                    break;
                case DarkYellow:
                    ansi.fgYellow();
                    break;
                case LightYellow:
                    ansi.fgBrightYellow();
                    break;
                case DarkBlue:
                    ansi.fgBlue();
                    break;
                case LightBlue:
                    ansi.fgBrightBlue();
                    break;
                case DarkMagenta:
                    ansi.fgMagenta();
                    break;
                case LightMagenta:
                    ansi.fgBrightMagenta();
                    break;
                case DarkCyan:
                    ansi.fgCyan();
                    break;
                case LightCyan:
                    ansi.fgBrightCyan();
                    break;
                case DarkGray:
                    ansi.fgBrightBlack();
                    break;
                case LightGray:
                    ansi.fg(Ansi.Color.WHITE);
                    break;
            }
            latestColorUsed = pNodeColor;
        }
        return ansi;
    }



    public void newLine()
    {
        if (colorWasUsed) {
            if (latestColorUsed != NodeColor.None) {
                ansi.reset();
                latestColorUsed = NodeColor.None;
            }
            ansi.newline();
        }
        else {
            sb.append(System.lineSeparator());
        }
    }



    public String finishUp()
    {
        if (colorWasUsed) {
            return ansi.toString();
        }
        return sb.toString();
    }
}
