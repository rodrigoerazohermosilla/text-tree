/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.api;

import org.barfuin.texttree.api.color.NoColorScheme;
import org.barfuin.texttree.api.style.AnnotationPosition;
import org.barfuin.texttree.api.style.TreeStyles;
import org.junit.Assert;
import org.junit.Test;


/**
 * Unit tests of {@link TreeOptions}.
 */
public class TreeOptionsTest
{
    @Test
    public void testPadding0()
    {
        TreeOptions underTest = new TreeOptions();
        underTest.setPadding(0);
        Assert.assertEquals("", underTest.getPaddingStr());
        Assert.assertEquals(0, underTest.getPadding());
    }



    @Test
    public void testPadding10()
    {
        TreeOptions underTest = new TreeOptions();
        underTest.setPadding(10);
        Assert.assertEquals("          ", underTest.getPaddingStr());
        Assert.assertEquals(10, underTest.getPadding());
    }



    @Test
    public void testPaddingNegative()
    {
        TreeOptions underTest = new TreeOptions();
        try {
            underTest.setPadding(-1);
            Assert.fail("expected exception was not thrown");
        }
        catch (IllegalArgumentException e) {
            // expected
        }
    }



    @Test
    public void testDefaultSettingsWithNull_cycleProtection()
    {
        TreeOptions underTest = new TreeOptions();
        underTest.setCycleProtection(null);
        Assert.assertEquals(CycleProtection.On, underTest.getCycleProtection());
    }



    @Test
    public void testDefaultSettingsWithNull_annotationPosition()
    {
        TreeOptions underTest = new TreeOptions();
        underTest.setAnnotationPosition(null);
        Assert.assertEquals(AnnotationPosition.NextLine, underTest.getAnnotationPosition());
    }



    @Test
    public void testDefaultSettingsWithNull_identityScheme()
    {
        TreeOptions underTest = new TreeOptions();
        underTest.setIdentityScheme(null);
        Assert.assertEquals(IdentityScheme.ByIdentity, underTest.getIdentityScheme());
    }



    @Test
    public void testDefaultSettingsWithNull_style()
    {
        TreeOptions underTest = new TreeOptions();
        underTest.setStyle(null);
        Assert.assertSame(TreeStyles.ASCII_ROUNDED, underTest.getStyle());
    }



    @Test
    public void testDefaultSettingsWithNull_colorScheme()
    {
        TreeOptions underTest = new TreeOptions();
        underTest.setColorScheme(null);
        Assert.assertTrue(underTest.getColorScheme() instanceof NoColorScheme);
    }



    @Test
    public void testInvalidMaxDepth()
    {
        TreeOptions underTest = new TreeOptions();
        try {
            underTest.setMaxDepth(-1);
            Assert.fail("expected exception was not thrown");
        }
        catch (IllegalArgumentException e) {
            // expected
        }
    }



    @Test
    public void testNonDefaultColorScheme()
    {
        TreeOptions underTest = new TreeOptions();
        underTest.setEnableDefaultColoring(false);
        Assert.assertTrue(underTest.getColorScheme() instanceof NoColorScheme);
    }
}
