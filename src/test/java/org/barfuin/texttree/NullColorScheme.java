/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree;

import org.barfuin.texttree.api.color.ColorScheme;
import org.barfuin.texttree.api.color.NodeColor;


/**
 * Color scheme in which all colors are <code>null</code>. Should behave like the
 * {@link org.barfuin.texttree.api.color.NoColorScheme NoColorScheme}.
 */
public class NullColorScheme
    implements ColorScheme
{
    @Override
    public NodeColor getEdgeColor()
    {
        return null;
    }



    @Override
    public NodeColor getTextColor()
    {
        return null;
    }



    @Override
    public NodeColor getAnnotationColor()
    {
        return null;
    }



    @Override
    public NodeColor getCalloutErrorColor()
    {
        return null;
    }



    @Override
    public NodeColor getCalloutNoteColor()
    {
        return null;
    }
}
