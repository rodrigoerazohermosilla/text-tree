/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree;

import java.util.ArrayList;
import java.util.List;

import org.barfuin.texttree.api.Node;
import org.barfuin.texttree.api.color.NodeColor;


/**
 * A simple implementation of a node that we can render, for test purposes.
 */
public class TestNode
    implements Node
{
    private final String text;

    private final String key;

    private Iterable<Node> children = new ArrayList<>();

    private NodeColor color = null;

    private String annotation = null;



    public TestNode(final String pText)
    {
        text = pText;
        key = pText;
    }



    public TestNode(final String pText, final String pKey)
    {
        text = pText;
        key = pKey;
    }



    @Override
    public Iterable<Node> getChildren()
    {
        return children;
    }



    public void addChild(final Node pChild)
    {
        if (children instanceof List<?>) {
            ((List<Node>) children).add(pChild);
        }
        else {
            throw new IllegalStateException("children are not a list");
        }
    }



    public void setChildren(final Iterable<Node> pChildren)
    {
        children = pChildren;
    }



    @Override
    public String getText()
    {
        return text;
    }



    @Override
    public String getKey()
    {
        return key;
    }



    @Override
    public NodeColor getColor()
    {
        return color;
    }



    public void setColor(final NodeColor pColor)
    {
        color = pColor;
    }



    @Override
    public String getAnnotation()
    {
        return annotation;
    }



    public void setAnnotation(final String pAnnotation)
    {
        annotation = pAnnotation;
    }
}
