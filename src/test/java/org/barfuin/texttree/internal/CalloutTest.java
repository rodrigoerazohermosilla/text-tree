/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;


/**
 * Unit tests of {@link Callout}.
 */
public class CalloutTest
{
    @Test
    public void testResourceBundle()
    {
        final List<String> expected = Arrays.asList("", "shown before", "CYCLE", "max. depth exceeded");
        final List<String> actual = new ArrayList<>();
        for (Callout underTest : Callout.values()) {
            Assert.assertNotNull(underTest.getElementType());
            Assert.assertNotNull(underTest.getText());
            actual.add(underTest.getText());
        }
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testResourceBundleDE()
    {
        final Locale orgLocale = Locale.getDefault();
        try {
            Locale.setDefault(Locale.GERMANY);
            final List<String> expected = Arrays.asList("", "Wiederholung", "ZYKLUS", "max. Tiefe überschritten");
            final List<String> actual = new ArrayList<>();
            for (Callout underTest : Callout.values()) {
                Assert.assertNotNull(underTest.getElementType());
                Assert.assertNotNull(underTest.getText());
                actual.add(underTest.getText());
            }
            Assert.assertEquals(expected, actual);
        }
        finally {
            Locale.setDefault(orgLocale);
        }
    }
}
