/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.io.IOException;
import java.net.URISyntaxException;

import org.barfuin.texttree.NullColorScheme;
import org.barfuin.texttree.TestColorScheme;
import org.barfuin.texttree.TestNode;
import org.barfuin.texttree.TestTreeFactory;
import org.barfuin.texttree.TestUtil;
import org.barfuin.texttree.api.TreeOptions;
import org.barfuin.texttree.api.color.NodeColor;
import org.junit.Assert;
import org.junit.Test;


/**
 * Some unit tests of {@link TextTreeImpl} which work specifically with colored tree elements.
 */
public class ColorTest
{
    @Test
    public void testSimpleTreeColoredNodes()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        setSomeNodesToRed(tree);
        TextTreeImpl underTest = new TextTreeImpl();

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected1-someColoredNodes.txt");
        Assert.assertEquals(expected, actual);
    }



    @SuppressWarnings("ConstantConditions")
    private void setSomeNodesToRed(final TestNode pNode)
    {
        if (pNode.getText() != null && pNode.getText().endsWith("2")) {
            pNode.setColor(NodeColor.LightRed);
        }
        if (TextTreeImpl.hasChildren(pNode)) {
            pNode.getChildren().forEach(child -> setSomeNodesToRed((TestNode) child));
        }
    }



    @Test
    public void testSimpleTreeAnnotatedDefaultColors()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TextTreeImplTest.annotateSomeNodes(tree);
        TreeOptions options = new TreeOptions();
        options.setEnableDefaultColoring(true);
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected1-annotated-defColor.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeCustomColorScheme()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSmallTree();
        TreeOptions options = new TreeOptions();
        options.setColorScheme(new TestColorScheme());
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected4-customColors.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeNullColorScheme()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSmallTree();
        TreeOptions options = new TreeOptions();
        options.setColorScheme(new NullColorScheme());
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected4-noColors.txt");
        Assert.assertEquals(expected, actual);
    }
}
