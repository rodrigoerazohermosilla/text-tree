/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.io.IOException;
import java.net.URISyntaxException;

import org.barfuin.texttree.TestUtil;
import org.barfuin.texttree.api.TreeOptions;
import org.barfuin.texttree.api.color.NodeColor;
import org.junit.Assert;
import org.junit.Test;


/**
 * Unit tests of {@link ColorizableAppender}.
 */
public class ColorizableAppenderTest
{
    @Test
    public void testColorOverrides()
        throws IOException, URISyntaxException
    {
        final ColorizableAppender underTest = new ColorizableAppender(new TreeOptions());

        for (NodeColor color : NodeColor.values()) {
            underTest.append(TreeElementType.Text, color.toString(), color);
            underTest.newLine();
        }

        final String actual = underTest.finishUp();
        final String expected = TestUtil.file2String("internal/expected-colors.txt");
        Assert.assertEquals(expected, actual);
    }
}
