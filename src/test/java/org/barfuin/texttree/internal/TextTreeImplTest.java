/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree.internal;

import java.io.IOException;
import java.net.URISyntaxException;

import org.barfuin.texttree.TestNode;
import org.barfuin.texttree.TestTreeFactory;
import org.barfuin.texttree.TestUtil;
import org.barfuin.texttree.api.TextTree;
import org.barfuin.texttree.api.TreeOptions;
import org.barfuin.texttree.api.style.AnnotationPosition;
import org.barfuin.texttree.api.style.TreeStyles;
import org.junit.Assert;
import org.junit.Test;


/**
 * Unit tests of {@link TextTreeImpl}.
 *
 * @see ColorTest
 * @see MultilineTest
 * @see NullInputTest
 */
public class TextTreeImplTest
{
    @Test
    public void testSimpleTreeAscii()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TextTree underTest = TextTree.newInstance();

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected1.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeUnicode()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TreeOptions options = new TreeOptions();
        options.setStyle(TreeStyles.UNICODE);
        TextTree underTest = TextTree.newInstance(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected2.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeUnicodeRounded()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TreeOptions options = new TreeOptions();
        options.setStyle(TreeStyles.UNICODE_ROUNDED);
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected3.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeAnnotated()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        annotateSomeNodes(tree);
        TextTreeImpl underTest = new TextTreeImpl();

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected1-annotated.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeAnnotatedInline()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        annotateSomeNodes(tree);
        TreeOptions options = new TreeOptions();
        options.setAnnotationPosition(AnnotationPosition.Inline);
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected1-annotated-inline.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeAnnotatedDefaultColors()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        annotateSomeNodes(tree);
        TreeOptions options = new TreeOptions();
        options.setEnableDefaultColoring(true);
        options.setMaxDepth(0);
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected1-annotated-defColor.txt");
        Assert.assertEquals(expected, actual);
    }



    @SuppressWarnings("ConstantConditions")
    static void annotateSomeNodes(final TestNode pNode)
    {
        if ("A".equals(pNode.getText())) {
            pNode.setAnnotation("annotation");
        }
        else if ("B".equals(pNode.getText())) {
            pNode.setAnnotation("multi-line" + System.lineSeparator() + " annotation");  // mind the space!
        }
        else if ("B32".equals(pNode.getText())) {
            pNode.setAnnotation("more" + System.lineSeparator() + " multi-line" + System.lineSeparator()
                + " annotation");  // mind the space!
        }
        else if ("C2".equals(pNode.getText())) {
            pNode.setAnnotation("annotation");
        }
        if (TextTreeImpl.hasChildren(pNode)) {
            pNode.getChildren().forEach(child -> annotateSomeNodes((TestNode) child));
        }
    }



    @Test
    public void testSimpleTreeMaxDepth1()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        annotateSomeNodes(tree);
        TreeOptions options = new TreeOptions();
        options.setMaxDepth(1);
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected-maxDepth1.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testSimpleTreeMaxDepth2()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        annotateSomeNodes(tree);
        TreeOptions options = new TreeOptions();
        options.setMaxDepth(2);
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected-maxDepth2.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testPadding0()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TreeOptions options = new TreeOptions();
        options.setPadding(0);
        options.setStyle(TreeStyles.UNICODE);
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected-padding0.txt");
        Assert.assertEquals(expected, actual);
    }



    @Test
    public void testPadding3()
        throws IOException, URISyntaxException
    {
        TestNode tree = new TestTreeFactory().createSampleTree();
        TreeOptions options = new TreeOptions();
        options.setPadding(3);
        TextTreeImpl underTest = new TextTreeImpl(options);

        final String actual = underTest.render(tree);

        final String expected = TestUtil.file2String("internal/expected-padding3.txt");
        Assert.assertEquals(expected, actual);
    }
}
