/*
 * Copyright 2020 barfuin and the text-tree contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.texttree;

/**
 * Creates some simple trees for use by unit tests.
 */
public class TestTreeFactory
{
    /**
     * Our classic sample tree.
     *
     * @return just that
     */
    public TestNode createSampleTree()
    {
        TestNode root = new TestNode("ROOT");
        TestNode n1 = new TestNode("A");
        TestNode n2 = new TestNode("B");
        n2.addChild(new TestNode("B1"));
        TestNode n22 = new TestNode("B2");
        n22.addChild(new TestNode("B21"));
        n2.addChild(n22);
        TestNode n23 = new TestNode("B3");
        n23.addChild(new TestNode("B31"));
        n23.addChild(new TestNode("B32"));
        n2.addChild(n23);
        TestNode n3 = new TestNode("C");
        n3.addChild(new TestNode("C1"));
        n3.addChild(new TestNode("C2"));
        root.addChild(n1);
        root.addChild(n2);
        root.addChild(n3);
        return root;
    }



    /**
     * A small tree with a cycle in it and one annotation.
     *
     * @return just that
     */
    public TestNode createSmallTree()
    {
        TestNode root = new TestNode("ROOT");
        TestNode n1 = new TestNode("A");
        n1.setAnnotation("my annotation A");
        TestNode n2 = new TestNode("B");
        n2.addChild(new TestNode("B1"));
        TestNode n22 = new TestNode("B2");
        n22.addChild(new TestNode("B21"));
        n2.addChild(n22);
        TestNode n23 = new TestNode("B3");
        n23.addChild(n2);    // cycle!
        n23.addChild(new TestNode("B32"));
        n2.addChild(n23);
        root.addChild(n1);
        root.addChild(n2);
        return root;
    }



    /**
     * A tree with some multi-line node texts.
     *
     * @return just that
     */
    public TestNode createMultilineTree()
    {
        TestNode root = new TestNode("ROOT IN" + System.lineSeparator() + "MULTI-LINE FORM");
        TestNode n1 = new TestNode("A in" + System.lineSeparator() + "multi-line" + System.lineSeparator() + "form");
        TestNode n2 = new TestNode("B");
        n2.addChild(new TestNode("B1"));
        TestNode n22 = new TestNode("B2");
        n22.addChild(new TestNode("B21 in" + System.lineSeparator() + "multi-line form"));
        n2.addChild(n22);
        TestNode n23 = new TestNode("B3");
        n23.addChild(new TestNode("B31"));
        n23.addChild(new TestNode("B32"));
        n2.addChild(n23);
        TestNode n3 = new TestNode("C in" + System.lineSeparator() + "multi-line" + System.lineSeparator() + "form");
        n3.addChild(new TestNode("C1"));
        n3.addChild(new TestNode("C2 in multi-line" + System.lineSeparator() + "form"));
        root.addChild(n1);
        root.addChild(n2);
        root.addChild(n3);
        return root;
    }



    /**
     * A complex tree with all kinds of callouts.
     *
     * @return just that
     */
    public TestNode createComplexTree()
    {
        TestNode tree = new TestNode("ROOT");
        TestNode nodeA = new TestNode("A");
        TestNode nodeA2 = new TestNode("A2");
        nodeA.addChild(nodeA2);
        TestNode subTree = createLittleSubtree();
        nodeA2.addChild(subTree);
        TestNode nodeB = new TestNode("B");
        nodeB.addChild(subTree);
        nodeB.addChild(new TestNode("B2"));
        nodeB.addChild(tree);
        TestNode nodeC = new TestNode("C");
        nodeC.addChild(null);
        nodeC.addChild(new TestNode(null));
        nodeC.addChild(new TestNode("C3"));
        tree.addChild(nodeA);
        tree.addChild(nodeB);
        tree.addChild(nodeC);
        return tree;
    }



    /**
     * A little subtree that can be inserted into other trees.
     *
     * @return just that
     */
    public TestNode createLittleSubtree()
    {
        TestNode subTree = new TestNode("subTree");
        subTree.setAnnotation("subtree annotation");
        TestNode nodeSubA = new TestNode("subA");
        TestNode nodeSubA1 = new TestNode("subA1");
        TestNode nodeSubA2 = new TestNode("subA2");
        TestNode nodeSubB = new TestNode("subB");
        nodeSubA.addChild(nodeSubA1);
        nodeSubA.addChild(nodeSubA2);
        subTree.addChild(nodeSubA);
        subTree.addChild(nodeSubB);
        return subTree;
    }
}
